# Simple Site using Senna.JS
Hello this is a simple site using senna.js, you can see the documentation below
https://sennajs.com/docs

# How to run
You can run this website use live server extension in VS code or you can use npm live server

# Demo
https://teguh02-sennajs.netlify.app/index.html

# Download
https://mega.nz/file/DChTBI6S#yM5FUIalSSn9rZSURmF491MwC6z4G8EJPv5VOaYp-BY
